###
# #============================
# # Statefull server
# #============================
###

net = require('net')
DEFAULT_SERVER_PORT = 8124

order_opened = false
items = []

server = net.createServer((c) ->
  console.log 'socket opened'
  c.setEncoding 'utf8'

  c.on 'end', ->
    console.log 'connection/socket closed'
    return

  c.on 'data', (data) ->
    console.log 'Incoming data: ' + data
    response = parseCommand(data)
    c.write response + '\n'

  make_array = (command) ->
    return command.trim().split ' '


  parseCommand = (_temp_items) ->
    input = make_array(_temp_items)

    command = input[0]
    item = input[1]

    switch command
      when 'open'
        if order_opened
          return "Error. Order already opened"
        else
          order_opened = true

        return "OK. Order opened."
      when 'process'
        if not order_opened
          return "Error. No order opened."

        if items.length == 0
          return "Error. No items in opened order."

        _temp_items = items
        order_opened = false
        items = []

        return "OK. Processing order with items " + _temp_items + "."

      when 'add'
        if not order_opened
          return "Error. No order opened."

        items.push item
        return "OK. Item " + item + " added."

      when ''
        return ''
      else
        return 'Error. Usage: open <order_name>, add <order_name> <product_name>}, process <order_name>'
        break
    return

  return
)
server.listen DEFAULT_SERVER_PORT, ->
  console.log 'server started on port ' + DEFAULT_SERVER_PORT