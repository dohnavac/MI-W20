orders = []


error = (msg) ->
  return "Error. " + msg

ok = (msg) ->
  return "Ok. " + msg

sanitize = (key) ->
  if key?
    return key.trim().toLocaleLowerCase()
  else
    return undefined

module.exports =
  open: (order) ->
    order = sanitize(order)

    if not order?
      return error("Specify order")

    if order not of orders
      orders[order] = {
        name: order
        items: []
      }

      return ok(order + " opened.")
    else
      return error("Order " + order + " already opened")


  process: (order) ->
    order = sanitize(order)

    if not order?
      return error("Specify order")

    if order not of orders
      return error("Unknown order " + order)

    if orders[order].items.length > 0
      _pp_items = orders[order].items
    else
      _pp_items = "~ empty order ~"

    data = ok("Processing " + order + ", Items: " + _pp_items)
    orders = orders.filter (word) -> word isnt order

    return data


  add: (order, item) ->
    order = sanitize(order)

    if not order?
      return error("Specify order")

    if order not of orders
      return error("Unknown order " + order)

    orders[order].items.push item

    return ok("Added item " + item)