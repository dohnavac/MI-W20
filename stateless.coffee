###
# #============================
# # Stateless server
# #============================
###

net = require('net')
order_storage = require('./orders')
DEFAULT_SERVER_PORT = 8124


server = net.createServer((c) ->
  console.log 'socket opened'
  c.setEncoding 'utf8'

  c.on 'end', ->
    console.log 'connection/socket closed'
    return

  c.on 'data', (data) ->
    console.log 'Incoming data: ' + data
    response = parseCommand(data)
    c.write response + '\n'

  sanitize = (command) ->
    command.trim().split ' '

  parseCommand = (ii) ->
    input = sanitize(ii)

    console.log input

    command = input[0]

    args = input.slice(1)
    order_name = args[0]
    item = args[1]

    switch command
      when 'open'
        return order_storage.open(order_name)
      when 'process'
        return order_storage.process(order_name)
      when 'add'
        return order_storage.add(order_name, item)
      when ''
        return ''
      else
        return 'Error. Usage: open <order_name>, add <order_name> <product_name>}, process <order_name>'
        break
    return

  return
)
server.listen DEFAULT_SERVER_PORT, ->
  console.log 'server started on port ' + DEFAULT_SERVER_PORT